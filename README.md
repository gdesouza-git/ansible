## A10-NetScaler-V01

### Descrição

O playbook A10-NetScaler-V01 automatiza a migração de ADCs da A10 Networks para o NetScaler da Citrix. O objetivo é facilitar a transição a criação dos arquivos de configuração essas plataformas, permitindo uma migração eficiente.

### extra-vars

As variáveis adicionais (`extra-vars`) são essenciais para personalizar a execução do playbook. Aqui está a lista de variáveis e suas finalidades:

- `startupConfig`: Caminho para o arquivo de configuração inicial do ADC A10.
- `ttpTemplate`: Template TTP (Text Template Parser) utilizado para extrair variáveis do `startupConfig`.
- `jinjaTemplate`: Template Jinja para criar o arquivo de configuração do NetScaler com base nas variáveis extraídas do `startupConfig`.

### Exemplo de extra-vars:

```bash
--extra-vars "startupConfig=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V01/files/startup-config jinjaTemplate=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V01/templates/a10-netscaler.j2 ttpTemplate=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V01/templates/a10-netscaler.ttp"

```

## A10-NetScaler-V02

### Descrição

Este playbook, A10-NetScaler-V02, estende as funcionalidades do V01 para incluir configurações específicas, como Content Switch e Load Balancing.

### extra-vars

Além das variáveis do V01, o V02 introduz duas novas variáveis relacionadas às configurações específicas do ADC:

- `jinjaTemplateCS`: Template Jinja para criar o arquivo de configuração do Content Switch.
- `jinjaTemplateLB`: Template Jinja para criar o arquivo de configuração do Load Balancing.

### Exemplo de extra-vars:

```bash
--extra-vars "startupConfig=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V02/files/configInput?ref_type=heads templateJinjaNetscaler=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V02/templates/a10-ns.j2?ref_type=heads templateTTPA10=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V02/templates/a10-ns.ttp?ref_type=heads templateJinjaCS=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V02/templates/citrix-teste-content-switching.j2?ref_type=heads templateJinjaLB=https://gitlab.com/gdesouza-git/ansible/-/raw/main/A10-NetScaler-V02/templates/citrix-teste-load-balancing.j2?ref_type=heads"

```